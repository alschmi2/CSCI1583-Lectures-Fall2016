//A program that add 2 numbers
/*
This
is
a
comment
*/

import java.util.Scanner;

public class Addition
{
    public static void main(String[] args)
    {
      //create a scanner object for input
      Scanner input = new Scanner(System.in);
        
      //create variable to hold first number
      int number1;
      //create variable to hold second number
      int number2;
      //create variable to hold sum
      int sum;
      
      //Prompt user for number 1
      System.out.printf("Enter first number: ");
      //get input from user for number 1 and set it
      number1 = input.nextInt();
      
      //Prompt user for number 1
      System.out.printf("Enter second number: ");
      
      //get input from user for number 2 and set it
      number2 = input.nextInt();
      
      //add number 1 to number 2 and save to sum
      sum = number1 + number2;
      
      //print the result to the screen
      System.out.printf("The sum is %d\n", sum);
    }

}