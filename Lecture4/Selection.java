//A program that determines whether we passed a class

import java.util.Scanner;

public class Selection
{
    public static void main(String[] args)
    {
        //Declare a variable for a class grade
        int grade;
        
        //Setup a scanner for input
        Scanner keyboard = new Scanner(System.in);
        
        boolean repeat = true;
        while (repeat == true)
        {
        
            //Prompt the user for input
            System.out.print("Enter a grade: ");
            
            //Get the user input
            grade = keyboard.nextInt();
            
            if (grade >= 90)
            {
                //Report to user that they passed
                System.out.println("You got an A");
            }
            else if (grade >= 80)
            {
                //Report to user that they passed
                System.out.println("You got a B");
            }
            else if (grade >= 70)
            {
                //Report to user that they passed
                System.out.println("You got a C");
            }
            else if (grade >= 60)
            {
                //Report to user that they passed
                System.out.println("You got a D");
            }
            else
            {
                //Report to user that they passed
                System.out.println("You got a F");
            }
            
            System.out.println("Press 1 to enter a new grade, 0 to quit.");
            int response = keyboard.nextInt();
            repeat = (response == 1) ? true : false;
            
        }//end while
    }//end main
}//end class