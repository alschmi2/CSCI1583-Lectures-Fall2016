//Examples of the many different data types
public class DataTypes
{
    public static void main(String[] args)
    {
        //integer numerical data type
        int number1 = 1;
        System.out.printf("The value is %d\n",number1 );
        
        //fractional numerical data type
        double number2 = 1.0;
        System.out.printf("The value is %f\n",number2 );
        
        //data from keyboard
        char character = '1';
        System.out.printf("The value is %c\n",character );
        
        //Text data
        String text = "1";
        System.out.printf("The value is %s\n",text );
        
        //True/False data type
        boolean yesOrNo = true;
        System.out.printf("The value is %b\n",yesOrNo );
        
        //Example of operation that uses numerical data to get a T/F
        boolean gameOver = 5 != 5;
        System.out.printf("The value is %d\n",gameOver );
        
    }
}