/*
 * Guess the number and win the game
 */
 import java.util.Scanner;
 
 public class Guess1
 {
    public static void main(String[] args)
    {
        int number = 729;
        
        System.out.println("Guess a number between 1 - 1000");
        
        Scanner input = new Scanner(System.in);
        int guess = input.nextInt();
        
        if (guess == number)
        {
            System.out.println("You won");
        }
        else
        {
            System.out.println("You lost");
        }
    }
 }