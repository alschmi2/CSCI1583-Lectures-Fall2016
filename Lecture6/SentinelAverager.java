//Top: Determine the class average for quizzes
import java.util.Scanner;

public class SentinelAverager
{
    public static void main(String[] args)
    {
        //initialize sum to 0
        int sum = 0;
        
        //initialize count to 0
        int count = 0;
        
        //declare average
        double average;
        
        //declare a sentinel
        int grade;
        
        //initialize a scanner object
        Scanner input = new Scanner(System.in);
        //Prompt user a first time
        System.out.print("Enter a grade or -1 to quit:");
        grade = input.nextInt();
        
        //while grade is not -1
        while (grade != -1)
        {
            // add to sum variable the value of grade
            sum += grade;
            // add 1 to count variable
            count++;
            
            //Prompt user a first time
            System.out.print("Enter a grade or -1 to quit:");
            grade = input.nextInt();
            
        }
        
        if (count != 0)
        {
            //Average is sum / count
            average = (double)(sum) / count;
            //Print Average
            System.out.printf("The average is %.2f\n", average);
        }
    }

}




