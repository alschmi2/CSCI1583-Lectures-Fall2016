public class DoWhile
{
    public static void main(String[] args)
    {
        System.out.println("Before while loop");
        int i=10;
        while (i<10)
        {
            System.out.printf("%d ", i);
            i++;
        }
        System.out.println("After while loop");
        
        
        System.out.println("Before do-while loop");
        int j = 11;
        do
        {
            System.out.printf("%d\n", j);
            j++;
        }while (j < 10);
        System.out.println("After while loop");
    }
}