import java.util.Scanner;

public class Switch
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a value 1: ");
        int value1 = input.nextInt();
        
        System.out.print("Enter a value 2: ");
        int value2 = input.nextInt();
        
        System.out.print("Enter a operation +, -, /, *:");
        String operation = input.next();
        
        switch(operation)
        {
            case "+":
                {
                    System.out.printf("The result is %d\n", value1 + value2);
                }
                break;
            case "-":
                {
                     System.out.printf("The result is %d\n", value1 - value2);
                }
                break;
            case "*":
                {
                     System.out.printf("The result is %d\n", value1 * value2);
                }
                break;
            case "/":
                {
                     System.out.printf("The result is %d\n", value1 / value2);
                }
                break;
                
            default:
                {
                    System.out.println("No such case!");
                }
        }
    }
}