public class Test
{
    public static void main(String[] args)
    {
        System.out.println("Before");
        int x2 = multiplier(3);
        printText(x2);
        
        System.out.println("After");
    }
    
    public static void printText(int name)
    {
        System.out.printf("x is %d\n", name);

    }
    
    //pass in a value and its twice as large
    public static int multiplier(int x)
    {
        return x * 2;
    }
}