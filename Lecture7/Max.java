public class Max
{
    public static void main(String[] args)
    {
        int result = maxValue(34,5,28);
        System.out.printf("the max value is %d", result);
    }
    
    public static int maxValue(int x, int y, int z)
    {
        int max = x;
        if (y > max)
        {
            max = y;
        }
        if (z > max)
        {
            max = z;
        }
        
        return max;
    }
}